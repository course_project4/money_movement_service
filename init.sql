CREATE USER 'money_movement_user'@'%' IDENTIFIED BY 'Auth123';

drop database  if exists money_movement;
create database money_movement;

GRANT ALL PRIVILEGES ON money_movement.* TO 'money_movement_user'@'%';

USE money_movement;

create table if not exists `wallet` (
    id int not null auto_increment primary key,
    user_id varchar(255) not null unique,
    wallet_type varchar(255) not null,
    index(user_id)
);

create table if not exists `account` (
    id int not null auto_increment primary key,
    cents int not null default 0,
    account_type varchar(255) not null,
    wallet_id int not null,
    foreign key (wallet_id) references wallet(id)
);

create table if not exists `transaction` (
    id int not null auto_increment primary key,
    pid varchar(255) not null,
    src_user_id varchar(255) not null,
    dst_user_id varchar(255) not null,
    src_wallet_id int not null,
    dst_wallet_id int not null,
    src_account_id int not null,
    dst_account_id int not null,
    src_account_type varchar(255) not null,
    dst_account_type varchar(255) not null,
    final_dst_merchant_wallet_id int,
    amount int not null,
    index(pid)
);

-- merchant and customer wallets
insert into wallet (id, user_id, wallet_type) values (1, 'f', 'CUSTOMER');
insert into wallet (id, user_id, wallet_type) values (2, 'merchant_id', 'MERCHANT');

-- customer accounts
insert into account (cents, account_type, wallet_id) values (5000000, 'DEFAULT', 1);
insert into account (cents, account_type, wallet_id) values (0, 'PAYMENT', 1);

-- merchant accounts
insert into account (cents, account_type, wallet_id) values (0, 'INCOMING', 2);