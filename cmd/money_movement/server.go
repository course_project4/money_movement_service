package main

import (
	"database/sql"
	"fmt"
	"log"
	"net"

	mm "money_movement_service/internal/implementation"
	pb "money_movement_service/proto"

	_ "github.com/go-sql-driver/mysql"
	"google.golang.org/grpc"
)

const (
	dbDriver   = "mysql"
	dbUser     = "money_movement_user"
	dbPassword = "Auth123"
	dbName     = "money_movement"
)

var db *sql.DB

func main() {
	var err error

	// Open a database connection
	dsn := fmt.Sprintf("%s:%s@tcp(mysql-money-movement:3306)/%s", dbUser, dbPassword, dbName)
	db, err := sql.Open(dbDriver, dsn)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if err := db.Close(); err != nil {
			log.Printf("Error closing db: %s", err)
		}
	}()

	if err := db.Ping(); err != nil {
		log.Fatalf("Error pinging to the db: %v", err)
	}
	log.Println("pinged successfully")

	// grpc ledger setup
	grpServer := grpc.NewServer()
	moneyMovementImpl := mm.NewMoneyMevementImplementation(db)
	pb.RegisterMoneyMevementServiceServer(grpServer, moneyMovementImpl)

	// listen and serve
	listen, err := net.Listen("tcp", ":7000")
	if err != nil {
		log.Fatalf("failed to listen on port 7000 %v", err)
	}

	log.Printf("Server is listening at %s", listen.Addr())

	if err := grpServer.Serve(listen); err != nil {
		log.Fatalf("failed grpServer.Serve %v", err)
	}
}
