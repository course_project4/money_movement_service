package mm

import (
	"context"
	"database/sql"
	"errors"
	"log"

	"github.com/google/uuid"

	"money_movement_service/internal/producer"
	pb "money_movement_service/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	insertTransactionQuery = "insert into transaction(pid, src_user_id, dst_user_id, src_wallet_id, dst_wallet_id, src_account_id, dst_account_id, src_account_type, dst_account_type, final_dst_merchant_wallet_id, amount) values (?,?,?,?,?,?,?,?,?,?,?)"
	selectTransactionQuery = "select pid, src_user_id, dst_user_id, src_wallet_id, dst_wallet_id, src_account_id, dst_account_id, src_account_type, dst_account_type, final_dst_merchant_wallet_id, amount from transaction where pid=?"
)

type Implementation struct {
	db *sql.DB
	pb.UnimplementedMoneyMevementServiceServer
}

func NewMoneyMevementImplementation(db *sql.DB) *Implementation {
	return &Implementation{db: db}
}

func (i *Implementation) Authorize(ctx context.Context, authorizePayload *pb.AuthorizePayload) (*pb.AuthorizeResponse, error) {
	if authorizePayload.GetCurrency() != "USD" {
		log.Println("err 1: ", authorizePayload)
		return nil, status.Error(codes.InvalidArgument, "only accepts USD")
	}

	// begin transaction
	tx, err := i.db.Begin()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	merchantWallet, err := fetchWallet(tx, authorizePayload.MerchantWalletUserID)
	if err != nil {
		log.Println("fetchWallet 1: ", err.Error())
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	customerWallet, err := fetchWallet(tx, authorizePayload.CustomerWalletUserID)
	if err != nil {
		log.Println("fetchWallet 2: ", err.Error())
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	srcAccount, err := fetchAccount(tx, customerWallet.ID, "DEFAULT")
	if err != nil {
		log.Println("fetchAccount 1: ", err.Error())
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	dstAccount, err := fetchAccount(tx, customerWallet.ID, "PAYMENT")
	if err != nil {
		log.Println("fetchAccount 2: ", err.Error())
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	err = transfer(tx, srcAccount, dstAccount, authorizePayload.Cents)
	if err != nil {
		log.Println("transfer: ", err.Error())
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	pid := uuid.NewString()
	if err := createTransaction(tx, pid, srcAccount, dstAccount,
		customerWallet, customerWallet, merchantWallet, authorizePayload.Cents); err != nil {
		log.Println("createTransaction: ", err.Error())
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	// end transaction
	err = tx.Commit()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	log.Println("pid: ", pid)

	return &pb.AuthorizeResponse{Pid: pid}, nil
}

func (i *Implementation) Capture(ctx context.Context, capturePayload *pb.CapturePayLoad) (*emptypb.Empty, error) {
	// begin transaction
	tx, err := i.db.Begin()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	authorizeTransaction, err := fetchTransaction(tx, capturePayload.Pid)
	if err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	srcAccount, err := fetchAccount(tx, authorizeTransaction.dstAccountWalletID, "PAYMENT")
	if err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	dstMerchantAccount, err := fetchAccount(tx, authorizeTransaction.finalDstMerchantWalletID, "INCOMING")
	if err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	customerWallet, err := fetchWallet(tx, authorizeTransaction.srcUserID)
	if err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	merchantWallet, err := fetchWalletWitgWalletID(tx, authorizeTransaction.finalDstMerchantWalletID)
	if err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	if err := transfer(tx, srcAccount, dstMerchantAccount, authorizeTransaction.amount); err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	if err := createTransaction(tx, authorizeTransaction.pid, srcAccount, dstMerchantAccount,
		customerWallet, merchantWallet, merchantWallet, authorizeTransaction.amount); err != nil {
		if rollBackerr := tx.Rollback(); rollBackerr != nil {
			return nil, status.Error(codes.Internal, rollBackerr.Error())
		}
		return nil, err
	}

	// end transaction
	err = tx.Commit()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	producer.SendCaptureMessage(authorizeTransaction.pid, authorizeTransaction.srcUserID, authorizeTransaction.amount)

	return &emptypb.Empty{}, nil
}

func fetchWallet(tx *sql.Tx, userID string) (wallet, error) {
	var w wallet

	stmt, err := tx.Prepare("select id, user_id, wallet_type from wallet where user_id=?")
	if err != nil {
		return w, status.Error(codes.Internal, err.Error())
	}

	err = stmt.QueryRow(userID).Scan(&w.ID, &w.userID, &w.walletType)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return w, status.Error(codes.InvalidArgument, err.Error())
		}
		return w, status.Error(codes.Internal, err.Error())
	}

	return w, nil
}

func fetchWalletWitgWalletID(tx *sql.Tx, walletID int32) (wallet, error) {
	var w wallet

	stmt, err := tx.Prepare("select id, user_id, wallet_type from wallet where id=?")
	if err != nil {
		return w, status.Error(codes.Internal, err.Error())
	}

	err = stmt.QueryRow(walletID).Scan(&w.ID, &w.userID, &w.walletType)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return w, status.Error(codes.InvalidArgument, err.Error())
		}
		return w, status.Error(codes.Internal, err.Error())
	}

	return w, nil
}

func fetchAccount(tx *sql.Tx, walletID int32, accountType string) (account, error) {
	var a account

	stmt, err := tx.Prepare("select id, cents, account_type, wallet_id from account where wallet_id=? and account_type=?")
	if err != nil {
		return a, status.Error(codes.Internal, err.Error())
	}

	err = stmt.QueryRow(walletID, accountType).Scan(&a.ID, &a.cents, &a.accountType, &a.walletID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return a, status.Error(codes.InvalidArgument, err.Error())
		}
		return a, status.Error(codes.Internal, err.Error())
	}

	return a, nil
}

func transfer(tx *sql.Tx, srcAccount account, dstAccount account, amount int64) error {
	if srcAccount.cents < amount {
		return status.Error(codes.InvalidArgument, "not enough money")
	}

	// subtract money from src
	stmt, err := tx.Prepare("update account set cents=? where id=?")
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	_, err = stmt.Exec(srcAccount.cents-amount, srcAccount.ID)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	// add money from src
	stmt, err = tx.Prepare("update account set cents=? where id=?")
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	_, err = stmt.Exec(dstAccount.cents+amount, dstAccount.ID)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	return nil
}

func createTransaction(tx *sql.Tx, pid string, srcAccount, dstAccount account,
	srcWallet, dstWallet, finalDstWallet wallet, amount int64) error {

	stmt, err := tx.Prepare(insertTransactionQuery)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	_, err = stmt.Exec(
		pid, srcWallet.userID, dstWallet.userID, srcAccount.walletID, dstAccount.walletID, srcAccount.ID,
		dstAccount.ID, srcAccount.accountType, dstAccount.accountType, finalDstWallet.ID, amount)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	return nil
}

func fetchTransaction(tx *sql.Tx, pid string) (transaction, error) {
	var t transaction
	stmt, err := tx.Prepare(selectTransactionQuery)
	if err != nil {
		return t, status.Error(codes.Internal, err.Error())
	}

	err = stmt.QueryRow(pid).Scan(
		&t.pid,
		&t.srcUserID,
		&t.dstUserID,
		&t.srcAccountWalletID,
		&t.dstAccountWalletID,
		&t.srcAccountID,
		&t.dstAccountID,
		&t.srcAccountType,
		&t.dstAccountType,
		&t.finalDstMerchantWalletID,
		&t.amount,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return t, status.Error(codes.InvalidArgument, err.Error())
		}
		return t, status.Error(codes.Internal, err.Error())
	}

	return t, nil
}
