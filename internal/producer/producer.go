package producer

import (
	"encoding/json"
	"log"
	"os"
	"sync"
	"time"

	"github.com/IBM/sarama"
	"github.com/google/uuid"
)

const (
	emailTopic  = "email"
	ledgerTopic = "ledger"
)

type EmailMsg struct {
	OrderID string `json:"order_id"`
	UserID  string `json:"user_id"`
}

type LedgerMsg struct {
	OrderID   string `json:"order_id"`
	UserID    string `json:"user_id"`
	Amount    int64  `json:"amount"`
	Operation string `json:"operation"`
	Date      string `json:"date"`
}

func SendCaptureMessage(pid, userID string, amount int64) error {
	sarama.Logger = log.New(os.Stdout, "[sarama]", log.LstdFlags)
	// create a sync producer

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer([]string{"my-cluster-kafka-bootstrap:9092"}, config)
	if err != nil {
		log.Println("error on sarama.NewSyncProducer: ", err)
		return err
	}
	defer func() {
		if err := producer.Close(); err != nil {
			log.Println("closing kafka producer")
		}
	}()

	emailMessage := EmailMsg{
		OrderID: pid,
		UserID:  userID,
	}

	ledgerMessage := LedgerMsg{
		OrderID:   pid,
		UserID:    userID,
		Amount:    amount,
		Operation: "DEBIT",
		Date:      time.Now().Format("2006-01-02"),
	}

	var wg sync.WaitGroup
	wg.Add(2)
	go sendMessage(&wg, producer, emailMessage, emailTopic)
	go sendMessage(&wg, producer, ledgerMessage, ledgerTopic)
	wg.Wait()

	return nil
}

func sendMessage[T EmailMsg | LedgerMsg](wg *sync.WaitGroup, producer sarama.SyncProducer, msg T, topic string) {
	defer wg.Done()

	bytes, err := json.Marshal(msg)
	if err != nil {
		log.Println("failed to marshal JSON")
		return
	}

	message := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(uuid.NewString()),
		Value: sarama.ByteEncoder(bytes),
	}

	partition, offset, err := producer.SendMessage(message)
	if err != nil {
		log.Printf("Failed to send message to Kafka: %v", err)
		return
	}
	log.Printf("Message sent to partition: %d at offset: %d\n", partition, offset)
}
